package slashCommands

import (
	"io"
	"log"
	"os/exec"
	"strconv"
	"strings"

	"github.com/bwmarrin/discordgo"
)

func (sc *slashCommands) meme(s *discordgo.Session, i *discordgo.InteractionCreate) {
	message := "It's done!"
	for _, option := range i.Data.Options {
		memeInfo := sc.memeMap[option.Name]
		if memeInfo.MaxH == 0 {
			message = "this meme doesn't exist :thinking:"
			break
		}
		log.Printf("[meme %s] %s#%s -> %s\n", option.Name, i.Member.User.Username, i.Member.User.Discriminator, option.Options[0].StringValue())

		proc := exec.Command("convert", genArgs(memeInfo, option.Options[0].StringValue())...)

		procOut, err := proc.StdoutPipe()
		if err != nil {
			message = "Mh… I've got a problem :thinking:"
			log.Println("[ERROR]", err)
			break
		}
		procErr, err := proc.StderrPipe()
		if err != nil {
			log.Println("[ERROR]", err)
		}

		err = proc.Start()
		if err != nil {
			message = "Mh… I've got a problem :thinking:"
			log.Println("[ERROR]", err)
			break
		}

		go func(procErr io.ReadCloser) {
			procErrBytes, err := io.ReadAll(procErr)
			if err != nil {
				log.Println("[ERROR]", err)
			}
			if len(procErrBytes) > 0 {
				log.Println("[ERROR]", string(procErrBytes))
			}
		}(procErr)

		err = s.InteractionResponseEdit(sc.appId, i.Interaction, &discordgo.WebhookEdit{
			Content: message,
		})
		if err != nil {
			log.Println("[ERROR]", err)
		}

		_, err = s.ChannelFileSend(i.ChannelID, i.Member.User.Username+i.Member.User.Discriminator+".jpg", procOut)
		if err != nil {
			log.Println("[ERROR]", err)
		}

	}
}

func genArgs(memeInfo meme, sentence string) []string {
	var strX string
	var strY string

	sentence = memeInfo.Prefix + sentence
	name := strings.Split(sentence, " ")

	if memeInfo.X >= 0 {
		strX = "+" + strconv.Itoa(memeInfo.X)
	} else {
		strX = strconv.Itoa(memeInfo.X)
	}
	if memeInfo.Y >= 0 {
		strY = "+" + strconv.Itoa(memeInfo.Y)
	} else {
		strY = "-" + strconv.Itoa(memeInfo.Y)
	}

	color := "black"
	if len(memeInfo.Color) > 0 {
		color = memeInfo.Color
	}

	gravity := "North"
	if len(memeInfo.Gravity) > 0 {
		gravity = memeInfo.Gravity
	}

	args := []string{
		"templates/" + memeInfo.FileName,
		"-fill",
		color,
		"-gravity",
		gravity,
		"-font",
		"Noto.ttc",
		"-pointsize",
		strconv.Itoa(memeInfo.FontSize),
		"-annotate",
		strX + strY,
	}
	finalString := ""
	lineCount := 0
	for i := 0; i < len(name) && lineCount < memeInfo.MaxH; i++ {
		word := name[i]
		for len(name) > i+1 && len(word) < memeInfo.MaxW {
			word += " " + name[i+1]
			i++
		}
		finalString += word + "\n"
		lineCount++
	}

	return append(args, finalString, "-")
}
