package slashCommands

import (
	"fmt"
	"github.com/bwmarrin/discordgo"
	"log"
	"time"
)

const voteTimeout = 30

/**
vérifier que la personne à kick est bien dans le vocal
empecher la personne à kick de revenir dans les 5 minutes
kick la personne
**/
func (sc *slashCommands) votekick(s *discordgo.Session, i *discordgo.InteractionCreate) {
	log.Printf("[votekick] %s#%s -> %s#%s (init)\n", i.Member.User.Username, i.Member.User.Discriminator, i.Data.Options[0].UserValue(s).Username, i.Data.Options[0].UserValue(s).Discriminator)
	err := s.InteractionResponseEdit(sc.appId, i.Interaction, &discordgo.WebhookEdit{
		Content: "Préparation du vote",
	})
	if err != nil {
		log.Println("[ERROR]", err)
	}

	userToKick := i.Data.Options[0].UserValue(s)

	message := "[fake] Procédure d'expultion du vocal engagée contre %s.\nVous avez __%d secondes__ pour voter"
	voteMessage, err := s.ChannelMessageSend(i.ChannelID, fmt.Sprintf(message, userToKick.Mention(), voteTimeout))
	if err != nil {
		log.Println("[ERROR]", err)
		return
	}

	err = s.MessageReactionAdd(voteMessage.ChannelID, voteMessage.ID, "✅")
	if err != nil {
		log.Println("[ERROR]", err)
		return
	}

	var votes = 0
	var participants = 0
	var ratio float64
	var success = false

	for remaining := voteTimeout; remaining > 0; remaining -= 3 {
		voteMessage, err = s.ChannelMessageEdit(voteMessage.ChannelID, voteMessage.ID, fmt.Sprintf(message, userToKick.Mention(), remaining))
		if err != nil {
			log.Println("[ERROR]", err)
			return
		}

		time.Sleep(3 * time.Second)

		users, err := s.MessageReactions(voteMessage.ChannelID, voteMessage.ID, "✅", 100, "", "")
		if err != nil {
			log.Println("[ERROR]", err)
			return
		}

		participants = 0
		votes = 0

		guild, err := s.State.Guild(i.GuildID)
		if err != nil {
			log.Println("[ERROR]", err)
			return
		}

		voices := guild.VoiceStates
		for _, vocal := range voices {
			for _, user := range users {
				if vocal.UserID == user.ID && !user.Bot {
					votes++
				}

			}

			member, err := s.State.Member(vocal.GuildID, vocal.UserID)
			if err != nil {
				log.Println("[ERROR]", err)
				return
			}

			if !member.User.Bot {
				participants++
			}
		}

		ratio = float64(votes*100) / float64(participants)

		if votes > 2 && ratio > 50 {
			remaining = 0
			success = true
		}
	}

	if success {
		log.Printf("[votekick] %s#%s -> %s#%s (accepted)\n", i.Member.User.Username, i.Member.User.Discriminator, i.Data.Options[0].UserValue(s).Username, i.Data.Options[0].UserValue(s).Discriminator)
		message = fmt.Sprintf("L'utilisateur a été expulsé du vocal : %2d / %2d", votes, participants)

	} else if ratio > 50 {
		log.Printf("[votekick] %s#%s -> %s#%s (fail: not enough votes)\n", i.Member.User.Username, i.Member.User.Discriminator, i.Data.Options[0].UserValue(s).Username, i.Data.Options[0].UserValue(s).Discriminator)
		message = fmt.Sprintf("Il n'y a pas assez de participants à ce vote : %2d", votes)
	} else {
		log.Printf("[votekick] %s#%s -> %s#%s (fail: not majority)\n", i.Member.User.Username, i.Member.User.Discriminator, i.Data.Options[0].UserValue(s).Username, i.Data.Options[0].UserValue(s).Discriminator)
		message = fmt.Sprintf("Ce vote n'a pas atteint une majorité : %3.1f%%", ratio)
	}

	_, err = s.ChannelMessageSend(i.ChannelID, message)
	if err != nil {
		log.Println("[ERROR]", err)
	}
}
