package main

import (
	"Discord/Nestor/commands"
	"Discord/Nestor/slashCommands"
	"log"
	"os"
	"os/signal"

	"github.com/bwmarrin/discordgo"
	flag "github.com/spf13/pflag"
)

func main() {
	const (
		VERSION       = "1.4.0"
		CommandPrefix = "!?"
	)

	var (
		token string
		appID string
	)
	// -- log system init --
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	log.SetOutput(os.Stdout)

	// -- flags --
	flag.StringVarP(&token, "token", "t", os.ExpandEnv("$NESTOR_TOKEN"), "the token from discord")
	flag.StringVarP(&appID, "appid", "a", os.ExpandEnv("$NESTOR_APPID"), "application ID from discord")
	flag.Parse()

	if len(token) < 50 {
		log.Println("invalid token.\nUsage:")
		flag.PrintDefaults()
		os.Exit(1)
	}
	if len(appID) < 18 {
		log.Println("invalid appID.\nUsage:")
		flag.PrintDefaults()
		os.Exit(1)
	}

	log.Println("[flow] bot starting")

	bot, err := discordgo.New("Bot " + token)
	if err != nil {
		log.Fatalln("[FATAL]", err)
	}

	bot.Identify.Intents = discordgo.IntentsAll
	bot.StateEnabled = true

	err = bot.Open()
	if err != nil {
		log.Fatalln("[FATAL]", err)
	}
	defer bot.Close()

	err = bot.UpdateGameStatus(0, "version "+VERSION)
	if err != nil {
		log.Println("[ERROR]", err)
		return
	}

	sc := slashCommands.Init(bot, appID)
	bot.AddHandler(sc.Handle)

	cmds := commands.Init(CommandPrefix)
	bot.AddHandler(cmds.Handle)

	sigint := make(chan os.Signal)
	signal.Notify(sigint, os.Interrupt)
	<-sigint
	log.Println("[flow] bot stopping")
}
