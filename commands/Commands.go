package commands

import (
	"encoding/json"
	"github.com/bwmarrin/discordgo"
	"io/ioutil"
	"log"
	"strings"
	"time"
)

type commands struct {
	commandPrefix string
	memeMap       map[string]meme
}

func Init(commandPrefix string) *commands {
	log.Println("[flow] getting meme info")
	memefile, err := ioutil.ReadFile("templates/meme.json")
	if err != nil {
		log.Fatalln(err)
	}
	var memeMap map[string]meme
	err = json.Unmarshal(memefile, &memeMap)
	if err != nil {
		log.Fatalln(err)
	}

	return &commands{
		commandPrefix: commandPrefix,
		memeMap:       memeMap,
	}
}

func (cmds *commands) Handle(session *discordgo.Session, message *discordgo.MessageCreate) {
	if message.Author.Bot || !strings.HasPrefix(message.Content, cmds.commandPrefix) {
		return
	}

	command := strings.Split(strings.TrimPrefix(message.Content, cmds.commandPrefix), " ")

	switch command[0] {
	case "meme":
		cmds.Meme(session, message, command)
	case "about":
		cmds.About(session, message)

	default:
		sent, err := session.ChannelMessageSend(message.ChannelID, "tout doux papy")
		if err != nil {
			log.Println(err)
		} else {
			go cmds.autoRemoveMessage(session, sent.ChannelID, sent.ID)
		}

	}
}

func (cmds *commands) sendTempMessage(session *discordgo.Session, channelID, message string) {
	sent, err := session.ChannelMessageSend(channelID, message)
	if err != nil {
		log.Println(err)
	} else {
		go cmds.autoRemoveMessage(session, sent.ChannelID, sent.ID)
	}
	return
}

func (cmds *commands) autoRemoveMessage(session *discordgo.Session, channelID, messageID string) {
	time.Sleep(60 * time.Second)
	err := session.ChannelMessageDelete(channelID, messageID)
	if err != nil {
		log.Println(err)
	}
}
