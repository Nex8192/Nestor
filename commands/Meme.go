package commands

import (
	"github.com/bwmarrin/discordgo"
	"io"
	"log"
	"os/exec"
	"strconv"
	"strings"
)

func (cmds *commands) Meme(session *discordgo.Session, message *discordgo.MessageCreate, command []string) {
	if len(command) == 2 && command[1] == "help" {
		memesHelp := "Voici la liste des types possible pour la commande !meme"
		for name := range cmds.memeMap {
			memesHelp += "\n- " + name
		}
		cmds.sendTempMessage(session, message.ChannelID, memesHelp)

		return
	}

	if len(command) < 3 {
		cmds.sendTempMessage(session, message.ChannelID, "where syntaxe ? :thinking:\nSYNTAXE: `"+cmds.commandPrefix+command[0]+" type message`")
		return
	}

	sentence := strings.Join(command[2:], " ")

	memeInfo := cmds.memeMap[command[1]]
	if memeInfo.MaxH == 0 {
		cmds.sendTempMessage(session, message.ChannelID, "Je connais pas ce meme…\nSYNTAXE: `"+cmds.commandPrefix+command[0]+" type message`")
		return
	}
	log.Printf("[meme %s] %s#%s -> %s\n", command[1], message.Author.Username, message.Author.Discriminator, sentence)

	proc := exec.Command("convert", genArgs(memeInfo, sentence)...)

	procOut, err := proc.StdoutPipe()
	if err != nil {
		cmds.sendTempMessage(session, message.ChannelID, "Désolé, problème technique !")
		log.Println("[ERROR]", err)
		return
	}
	procErr, err := proc.StderrPipe()
	if err != nil {
		log.Println("[ERROR]", err)
	}

	err = proc.Start()
	if err != nil {
		cmds.sendTempMessage(session, message.ChannelID, "Désolé, problème technique !")
		log.Println("[ERROR]", err)
		return
	}

	go func(procErr io.ReadCloser) {
		procErrBytes, err := io.ReadAll(procErr)
		if err != nil {
			log.Println("[ERROR]", err)
		}
		if len(procErrBytes) > 0 {
			log.Println("[ERROR]", string(procErrBytes))
		}
	}(procErr)

	_, err = session.ChannelFileSend(message.ChannelID, message.Author.Username+message.Author.Discriminator+".jpg", procOut)
	if err != nil {
		log.Println("[ERROR]", err)
		return
	}

	cmds.autoRemoveMessage(session, message.ChannelID, message.ID)
}

func genArgs(memeInfo meme, sentence string) []string {
	var strX string
	var strY string

	sentence = memeInfo.Prefix + sentence
	name := strings.Split(sentence, " ")

	if memeInfo.X >= 0 {
		strX = "+" + strconv.Itoa(memeInfo.X)
	} else {
		strX = strconv.Itoa(memeInfo.X)
	}
	if memeInfo.Y >= 0 {
		strY = "+" + strconv.Itoa(memeInfo.Y)
	} else {
		strY = "-" + strconv.Itoa(memeInfo.Y)
	}

	color := "black"
	if len(memeInfo.Color) > 0 {
		color = memeInfo.Color
	}

	gravity := "North"
	if len(memeInfo.Gravity) > 0 {
		gravity = memeInfo.Gravity
	}

	args := []string{
		"templates/" + memeInfo.FileName,
		"-fill",
		color,
		"-gravity",
		gravity,
		"-font",
		"Noto.ttc",
		"-pointsize",
		strconv.Itoa(memeInfo.FontSize),
		"-annotate",
		strX + strY,
	}
	finalString := ""
	lineCount := 0
	for i := 0; i < len(name) && lineCount < memeInfo.MaxH; i++ {
		word := name[i]
		for len(name) > i+1 && len(word) < memeInfo.MaxW {
			word += " " + name[i+1]
			i++
		}
		finalString += word + "\n"
		lineCount++
	}

	return append(args, finalString, "-")
}
