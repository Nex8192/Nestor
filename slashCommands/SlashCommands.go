package slashCommands

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"time"

	"github.com/bwmarrin/discordgo"
)

type slashCommands struct {
	appId   string
	memeMap map[string]meme
}

func Init(bot *discordgo.Session, appId string) *slashCommands {

	// get existing slash-commands
	commands := make(map[string]*discordgo.ApplicationCommand)
	cmds, err := bot.ApplicationCommands(appId, "")
	if err != nil {
		log.Fatalln("[FATAL]", err)
	}
	for _, cmd := range cmds {
		commands[cmd.Name] = cmd
		log.Println(cmd.Name, cmd.ID)
	}

	log.Println("[flow] getting meme info")
	memefile, err := ioutil.ReadFile("templates/meme.json")
	if err != nil {
		log.Fatalln(err)
	}
	var memeMap map[string]meme
	err = json.Unmarshal(memefile, &memeMap)
	if err != nil {
		log.Fatalln(err)
	}

	// create the non-existent ones
	if commands["server"] == nil {
		log.Println("[flow] creating server command")
		_, err = bot.ApplicationCommandCreate(appId, "839522087118569502", &discordgo.ApplicationCommand{
			ID:          "server",
			Name:        "server",
			Description: "change things on the server",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Name:        "name",
					Description: "changes the server's name",
					Options: []*discordgo.ApplicationCommandOption{
						{
							Type:        discordgo.ApplicationCommandOptionString,
							Name:        "name",
							Description: "new name for the server",
							Required:    true,
						},
					},
				},
				/*{
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Name:        "pin",
					Description: "pin your last message",
				},
				{
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Name:        "unpin",
					Description: "unpin your last message",
				},*/
			},
		})
		if err != nil {
			log.Fatalln("[FATAL]", err)
		}
	}

	if commands["role"] == nil {
		log.Println("[flow] creating role command")
		_, err = bot.ApplicationCommandCreate(appId, "839522087118569502", &discordgo.ApplicationCommand{
			ID:          "role",
			Name:        "role",
			Description: "choose your roles",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Name:        "add",
					Description: "add yourself a new role",
					Options: []*discordgo.ApplicationCommandOption{
						{
							Type:        discordgo.ApplicationCommandOptionRole,
							Name:        "role",
							Description: "the role you want",
							Required:    true,
						},
					},
				},
				{
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Name:        "rm",
					Description: "remove your role",
					Options: []*discordgo.ApplicationCommandOption{
						{
							Type:        discordgo.ApplicationCommandOptionRole,
							Name:        "role",
							Description: "the role you want to remove",
							Required:    true,
						},
					},
				},
			},
		})
		if err != nil {
			log.Fatalln("[FATAL]", err)
		}
	}

	if commands["meme"] != nil && len(commands["meme"].Options) != len(memeMap) {
		err = bot.ApplicationCommandDelete(appId, "", commands["meme"].ID)
		if err != nil {
			log.Println(err)
		}

		commands["meme"] = nil
	}
	if commands["meme"] == nil {
		log.Println("[flow] creating meme command")

		options := make([]*discordgo.ApplicationCommandOption, 0, len(memeMap))

		for name := range memeMap {
			options = append(options, &discordgo.ApplicationCommandOption{
				Type:        discordgo.ApplicationCommandOptionSubCommand,
				Name:        name,
				Description: "make a " + name + " meme",
				Options: []*discordgo.ApplicationCommandOption{
					{
						Type:        discordgo.ApplicationCommandOptionString,
						Name:        "message",
						Description: "string",
						Required:    true,
					},
				},
			})
		}

		_, err = bot.ApplicationCommandCreate(appId, "", &discordgo.ApplicationCommand{
			ID:          "meme",
			Name:        "meme",
			Description: "make amazing memes",
			Options:     options,
		})
		if err != nil {
			log.Fatalln("[FATAL]", err)
		}
	}

	return &slashCommands{
		//bot: bot,
		appId:   appId,
		memeMap: memeMap,
	}
}

func (sc *slashCommands) Handle(s *discordgo.Session, i *discordgo.InteractionCreate) {

	err := s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
		Type: discordgo.InteractionResponseChannelMessageWithSource,
		Data: &discordgo.InteractionApplicationCommandResponseData{
			Content: ":notepad_spiral: noted",
			Flags:   1 << 6,
		},
	})

	if err != nil {
		log.Println(err)
	}

	go func(s *discordgo.Session, i *discordgo.Interaction) {
		time.Sleep(time.Second * 7)
		err := s.InteractionResponseDelete(sc.appId, i)
		if err != nil {
			log.Println(err)
		}
	}(s, i.Interaction)

	switch i.Data.Name {
	case "server":
		sc.server(s, i)
	case "role":
		sc.role(s, i)
	case "votekick":
		sc.votekick(s, i)
	case "countdown":
		sc.countdown(s, i)
	case "meme":
		sc.meme(s, i)
	case "nestor":
		sc.nestor(s, i)
	default:
		err := s.InteractionResponseEdit(sc.appId, i.Interaction, &discordgo.WebhookEdit{
			Content: "This command is unknown at the moment, maybe this bot has been updated a very short time ago\n**Please wait for around 15 minutes** and it should work as expected",
		})
		if err != nil {
			log.Println("[ERROR]", err)
			return
		}
	}
}
