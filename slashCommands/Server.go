package slashCommands

import (
	"log"

	"github.com/bwmarrin/discordgo"
)

func (sc *slashCommands) server(s *discordgo.Session, i *discordgo.InteractionCreate) {
	for _, option := range i.Data.Options {
		switch option.Name {
		case "name":
			sc.servername(s, i, option)
		case "pin":
			sc.serverpin(s, i, option)
		case "unpin":
			sc.serverunpin(s, i, option)
		}
	}
}

func (sc *slashCommands) servername(s *discordgo.Session, i *discordgo.InteractionCreate, subCommand *discordgo.ApplicationCommandInteractionDataOption) {
	log.Printf("[servername] %s#%s -> %s\n", i.Member.User.Username, i.Member.User.Discriminator, subCommand.Options[0].StringValue())

	isValid := true
	message := "It's done!"

	name := "B2: Sadique² & " + subCommand.Options[0].StringValue()
	_, err := s.GuildEdit(i.GuildID, discordgo.GuildParams{
		Name: name,
	})
	if err != nil {
		log.Println("[ERROR]", err)

		message = "Mh… I've got a problem :thinking:"
		isValid = false
	}
	err = s.InteractionResponseEdit(sc.appId, i.Interaction, &discordgo.WebhookEdit{
		Content: message,
	})
	if err != nil {
		log.Println("[ERROR]", err)
		isValid = false
	}

	if isValid {
		_, err = s.ChannelMessageSend("887367237978177537", "Nous avons un nouveau nom !\n**"+name+"**\n*− "+i.Member.User.Username+"#"+i.Member.User.Discriminator+"*")

		if err != nil {
			log.Println("[ERROR]", err)
			return
		}
	}
}

func (sc *slashCommands) serverpin(s *discordgo.Session, i *discordgo.InteractionCreate, subCommand *discordgo.ApplicationCommandInteractionDataOption) {
	log.Printf("[serverpin] %s#%s -> %s\n", i.Member.User.Username, i.Member.User.Discriminator, subCommand.StringValue())
	err := s.InteractionResponseEdit(sc.appId, i.Interaction, &discordgo.WebhookEdit{
		Content: "Not implemented yet",
	})
	if err != nil {
		log.Println("[ERROR]", err)
	}
}

func (sc *slashCommands) serverunpin(s *discordgo.Session, i *discordgo.InteractionCreate, subCommand *discordgo.ApplicationCommandInteractionDataOption) {
	log.Printf("[serverunpin] %s#%s -> %s\n", i.Member.User.Username, i.Member.User.Discriminator, subCommand.StringValue())
	err := s.InteractionResponseEdit(sc.appId, i.Interaction, &discordgo.WebhookEdit{
		Content: "Not implemented yet",
	})
	if err != nil {
		log.Println("[ERROR]", err)
	}
}
