all: compile

compile:
	export CGO_ENABLED=0;go build -ldflags "-s -w" -o nestor
	strip nestor

prod: compile
	upx --brute nestor

send: compile
	scp nestor scp://server.vpn
	ssh server.vpn 'sudo mv nestor /usr/bin/nestor && sudo chown root:root /usr/bin/nestor && sudo chmod 755 /usr/bin/nestor && sudo systemctl restart nestor && sleep 3 && sudo systemctl status nestor'

deps:
	go get "github.com/bwmarrin/discordgo"
	go get "github.com/spf13/pflag"
