package commands

type meme struct {
	FileName string `json:"file_name"`
	X        int    `json:"x"`
	Y        int    `json:"y"`
	FontSize int    `json:"font_size"`
	MaxW     int    `json:"max_w"`
	MaxH     int    `json:"max_h"`
	Prefix   string `json:"prefix,omitempty"`
	Color    string `json:"color,omitempty"`
	Gravity  string `json:"gravity,omitempty"`
}
