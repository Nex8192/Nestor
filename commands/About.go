package commands

import "github.com/bwmarrin/discordgo"

func (cmds *commands) About(session *discordgo.Session, message *discordgo.MessageCreate) {
	cmds.sendTempMessage(session, message.ChannelID, "**Nestor**\n\nDev par *Nex*\nCode source : https://framagit.org/Nex8192/Nestor\n")
}
