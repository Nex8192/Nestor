package slashCommands

import (
	"log"

	"github.com/bwmarrin/discordgo"
)

func (sc *slashCommands) role(s *discordgo.Session, i *discordgo.InteractionCreate) {
	for _, option := range i.Data.Options {
		switch option.Name {
		case "add":
			sc.addRole(s, i, option)

		case "rm":
			sc.rmRole(s, i, option)
		}
	}
}

func (sc *slashCommands) addRole(s *discordgo.Session, i *discordgo.InteractionCreate, subCommand *discordgo.ApplicationCommandInteractionDataOption) {
	var message string
	for _, option := range subCommand.Options {
		role := option.RoleValue(s, i.GuildID)
		log.Printf("[role add] %s#%s -> %s\n", i.Member.User.Username, i.Member.User.Discriminator, role.Name)
		message += "\nAdding " + role.Name + " : "

		if role.Permissions&discordgo.PermissionAdministrator == discordgo.PermissionAdministrator ||
			role.Permissions&discordgo.PermissionManageChannels == discordgo.PermissionManageChannels ||
			role.Permissions&discordgo.PermissionManageRoles == discordgo.PermissionManageRoles ||
			role.ID == "887079226820399198" || role.ID == "887091661354696705" {
			message += "don't even try:rage:"
		} else if role.Name == "@everyone" {
			message += "Eh… I think you already have this role…"
		} else {
			err := s.GuildMemberRoleAdd(i.GuildID, i.Member.User.ID, role.ID)
			if err != nil {
				log.Printf("[ERROR] %s#%s : %v\n", i.Member.User.Username, i.Member.User.Discriminator, err)
				message += "FAIL:no_entry:"
			} else {
				message += "OK:white_check_mark:"
			}
		}

		err := s.InteractionResponseEdit(sc.appId, i.Interaction, &discordgo.WebhookEdit{
			Content: message,
		})
		if err != nil {
			log.Println("[ERROR]", err)
		}
	}
}

func (sc *slashCommands) rmRole(s *discordgo.Session, i *discordgo.InteractionCreate, subCommand *discordgo.ApplicationCommandInteractionDataOption) {
	var message string
	for _, option := range subCommand.Options {
		role := option.RoleValue(s, i.GuildID)
		log.Printf("[role rm] %s#%s -> %s\n", i.Member.User.Username, i.Member.User.Discriminator, role.Name)
		message += "\nRemoving " + role.Name + " : "

		err := s.GuildMemberRoleRemove(i.GuildID, i.Member.User.ID, role.ID)
		if err != nil {
			log.Printf("[ERROR] %s#%s : %v\n", i.Member.User.Username, i.Member.User.Discriminator, err)
			message += "FAIL:no_entry:"
		} else {
			message += "OK:white_check_mark:"
		}
		err = s.InteractionResponseEdit(sc.appId, i.Interaction, &discordgo.WebhookEdit{
			Content: message,
		})
		if err != nil {
			log.Println("[ERROR]", err)
		}
	}
}
