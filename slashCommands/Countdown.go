package slashCommands

import (
	"github.com/bwmarrin/discordgo"
	"log"
	"strconv"
	"strings"
)

func (sc *slashCommands) countdown(s *discordgo.Session, i *discordgo.InteractionCreate) {
	message := ""
	timeoutStr := i.Data.Options[0].StringValue()
	timeoutSplit := strings.Split(timeoutStr, ":")
	timeoutInt := make([]uint8, 0, 3)

	for _, val := range timeoutSplit {
		valInt, err := strconv.ParseUint(val, 10, 8)
		if err != nil {
			log.Println("[ERROR]", err)
			message = "'" + val + "' : not a number in 0-255"
			break
		}

		timeoutInt = append(timeoutInt, uint8(valInt))
	}

	if len(message) != 0 {
		err := s.InteractionResponseEdit(sc.appId, i.Interaction, &discordgo.WebhookEdit{
			Content: message,
		})
		if err != nil {
			log.Println("[ERROR]", err)
		}
		return
	}

	//TODO: tant que temps restant != 0, actualiser

}
