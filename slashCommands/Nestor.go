package slashCommands

import (
	"log"

	"github.com/bwmarrin/discordgo"
)

func (sc *slashCommands) nestor(s *discordgo.Session, i *discordgo.InteractionCreate) {
	err := s.InteractionResponseEdit(sc.appId, i.Interaction, &discordgo.WebhookEdit{
		Content: "no",
	})
	if err != nil {
		log.Println("[ERROR]", err)
	}
}
